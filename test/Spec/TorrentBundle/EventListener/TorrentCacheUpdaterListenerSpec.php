<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\EventListener;

use PhpSpec\ObjectBehavior;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Event\TorrentAfterEvent;
use TorrentBundle\EventListener\TorrentCacheUpdaterListener;

class TorrentCacheUpdaterListenerSpec extends ObjectBehavior
{
    public function let(
        CacheInterface $cache,

        TorrentAfterEvent $torrentAfterEvent,
        TorrentInterface $torrent
    ) {
        $torrent->getHash()->willReturn('hash');
        $torrentAfterEvent->getTorrent()->willReturn($torrent);

        $cache->isUpToDate(CacheInterface::KEY_TORRENT_HASHES)->willReturn(false);

        $this->beConstructedWith($cache);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(TorrentCacheUpdaterListener::class);
    }

    public function it_stores_torrent_into_the_cache($torrent, $torrentAfterEvent, $cache)
    {
        $cache->set('hash', $torrent)->shouldBeCalledTimes(1);

        $this->afterTorrentUpdated($torrentAfterEvent)->shouldReturn($torrentAfterEvent);
    }

    public function it_removes_torrent_from_the_cache($torrentAfterEvent, $cache)
    {
        $cache->invalidate('hash')->shouldBeCalledTimes(1);

        $this->afterTorrentRemoved($torrentAfterEvent)->shouldReturn($torrentAfterEvent);
    }
}
