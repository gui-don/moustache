<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\EventListener;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Client\ClientInterface;
use TorrentBundle\Event\ClientAfterEvent;
use TorrentBundle\EventListener\ClientTokenCacheUpdaterListener;

class ClientTokenCacheUpdaterListenerSpec extends ObjectBehavior
{
    public function let(
        CacheInterface $cache,
        LoggerInterface $logger,

        ClientAfterEvent $event,
        ClientInterface $client
    ) {
        $client->getSessionToken()->willReturn('sessionTOKEN');
        $client->getName()->willReturn('client');
        $event->getClient()->willReturn($client);

        $cache->isUpToDate(CacheInterface::KEY_CLIENT_TOKEN)->willReturn(true);
        $cache->get(CacheInterface::KEY_CLIENT_TOKEN)->willReturn('sessionTOKEN');

        $this->beConstructedWith($cache, $logger);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(ClientTokenCacheUpdaterListener::class);
    }

    public function it_does_not_save_anything_when_cache_is_already_up_to_date_and_client_token_has_not_changed($cache, $event)
    {
        $cache->set(Argument::cetera())->shouldNotBeCalled();

        $this->afterClientRetrieved($event)->shouldReturn($event);
    }

    public function it_saves_client_token_in_cache_when_the_cache_is_not_up_to_date($cache, $event)
    {
        $cache->isUpToDate(CacheInterface::KEY_CLIENT_TOKEN)->willReturn(false);

        $cache->set(CacheInterface::KEY_CLIENT_TOKEN, 'sessionTOKEN', CacheInterface::EXPIRATION_CLIENT_TOKEN)->shouldBeCalledTimes(1);

        $this->afterClientRetrieved($event)->shouldReturn($event);
    }
}
