<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\EventListener;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Client\ClientInterface;
use TorrentBundle\Event\ClientAfterEvent;
use TorrentBundle\Event\TorrentAfterEvent;
use TorrentBundle\EventListener\TorrentHashCacheUpdaterListener;
use TorrentBundle\Exception\Client\CacheOutdatedException;
use TorrentBundle\Helper\TorrentClientHelper;

class TorrentHashCacheUpdaterListenerSpec extends ObjectBehavior
{
    public function let(
        TorrentClientHelper $torrentClientHelper,
        CacheInterface $cache,
        LoggerInterface $logger,

        ClientInterface $client,
        ClientAfterEvent $clientAfterEvent,
        TorrentAfterEvent $torrentAfterEvent,
        GetResponseForExceptionEvent $exceptionEvent
    ) {
        $client->getName()->willReturn('client');
        $torrentClientHelper->get()->willReturn($client);
        $torrentClientHelper->isEmpty()->willReturn(false);

        $cache->isUpToDate(CacheInterface::KEY_TORRENT_HASHES)->willReturn(false);

        $logger->info(Argument::type('string'))->willReturn();

        $exceptionEvent->getException()->willReturn(new CacheOutdatedException());

        $this->beConstructedWith($torrentClientHelper, $cache, $logger);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(TorrentHashCacheUpdaterListener::class);
    }

    public function it_does_not_update_torrent_hashes_cache_after_client_retreived_when_cache_is_already_up_to_date($clientAfterEvent, $client, $cache)
    {
        $cache->isUpToDate(CacheInterface::KEY_TORRENT_HASHES)->willReturn(true);

        $client->updateCache()->shouldNotBeCalled();

        $this->afterClientRetrieved($clientAfterEvent)->shouldReturn($clientAfterEvent);
    }

    public function it_does_not_update_torrent_hashes_cache_after_a_random_exception($exceptionEvent, $client)
    {
        $exceptionEvent->getException()->willReturn(new RuntimeException());

        $client->updateCache()->shouldNotBeCalled();

        $this->onKernelException($exceptionEvent)->shouldReturn($exceptionEvent);
    }

    public function it_updates_torrent_hashes_cache_after_torrent_added($torrentAfterEvent, $client)
    {
        $client->updateCache()->shouldBeCalledTimes(1);

        $this->afterTorrentAdded($torrentAfterEvent)->shouldReturn($torrentAfterEvent);
    }

    public function it_updates_torrent_hashes_cache_after_torrent_removed($torrentAfterEvent, $client)
    {
        $client->updateCache()->shouldBeCalledTimes(1);

        $this->afterTorrentRemoved($torrentAfterEvent)->shouldReturn($torrentAfterEvent);
    }

    public function it_updates_torrent_hashes_cache_after_client_retreived($clientAfterEvent, $client)
    {
        $client->updateCache()->shouldBeCalledTimes(1);

        $this->afterClientRetrieved($clientAfterEvent)->shouldReturn($clientAfterEvent);
    }

    public function it_updates_torrent_hashes_cache_after_a_cache_outdated_exception($exceptionEvent, $client)
    {
        $client->updateCache()->shouldBeCalledTimes(1);

        $this->onKernelException($exceptionEvent)->shouldReturn($exceptionEvent);
    }
}
