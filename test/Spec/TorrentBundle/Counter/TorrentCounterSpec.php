<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\Counter;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TorrentBundle\Cache\TorrentCounterCacheProxy;
use TorrentBundle\Client\Client;
use TorrentBundle\Counter\TorrentCounter;
use TorrentBundle\Entity\Torrent;

class TorrentCounterSpec extends ObjectBehavior
{
    public function let(
        TorrentCounterCacheProxy $torrentCounterCacheProxy,
        Client $torrentClient,

        Torrent $torrent,
        Torrent $torrent2
    ) {
        $torrentCounterCacheProxy->isUpToDate(Argument::type('string'))->willReturn(true);
        $torrentCounterCacheProxy->get(Argument::type('string'))->willReturn(5);
        $torrentCounterCacheProxy->set(Argument::cetera())->will(function ($args) {
            return $args[1];
        });

        $torrent->isFailed()->willReturn(true);
        $torrent2->isFailed()->willReturn(true);
        $torrent->isStarted()->willReturn(false);
        $torrent2->isStarted()->willReturn(false);
        $torrent->isStopped()->willReturn(true);
        $torrent2->isStopped()->willReturn(false);

        $torrentClient->getAll()->willReturn([$torrent, $torrent2]);

        $this->beConstructedWith($torrentCounterCacheProxy, $torrentClient);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(TorrentCounter::class);
    }

    public function it_gets_total_torrents_in_failed_state_by_looking_into_cache()
    {
        $this->getFailed()->shouldReturn(5);
    }

    public function it_gets_total_torrents_in_started_state_by_looking_into_cache()
    {
        $this->getStarted()->shouldReturn(5);
    }

    public function it_gets_total_torrents_in_stopped_state_by_looking_into_cache()
    {
        $this->getStopped()->shouldReturn(5);
    }

    public function it_gets_total_number_of_torrents_by_looking_into_cache()
    {
        $this->getTotal()->shouldReturn(5);
    }

    public function it_gets_total_torrents_in_failed_state_by_querying($torrentCounterCacheProxy)
    {
        $torrentCounterCacheProxy->isUpToDate(Argument::type('string'))->willReturn(false);

        $torrentCounterCacheProxy->set(Argument::type('string'), 2)->willReturn(2)->shouldBeCalledTimes(1);

        $this->getFailed()->shouldReturn(2);
    }

    public function it_gets_total_torrents_in_started_state_by_querying($torrentCounterCacheProxy)
    {
        $torrentCounterCacheProxy->isUpToDate(Argument::type('string'))->willReturn(false);

        $torrentCounterCacheProxy->set(Argument::type('string'), 0)->willReturn(0)->shouldBeCalledTimes(1);

        $this->getStarted()->shouldReturn(0);
    }

    public function it_gets_total_torrents_in_stopped_state_by_querying($torrentCounterCacheProxy)
    {
        $torrentCounterCacheProxy->isUpToDate(Argument::type('string'))->willReturn(false);

        $torrentCounterCacheProxy->set(Argument::type('string'), 1)->willReturn(1)->shouldBeCalledTimes(1);

        $this->getStopped()->shouldReturn(1);
    }

    public function it_gets_total_number_of_torrents_by_querying($torrentCounterCacheProxy)
    {
        $torrentCounterCacheProxy->isUpToDate(Argument::type('string'))->willReturn(false);

        $torrentCounterCacheProxy->set(Argument::type('string'), 2)->willReturn(2)->shouldBeCalledTimes(1);

        $this->getTotal()->shouldReturn(2);
    }
}
