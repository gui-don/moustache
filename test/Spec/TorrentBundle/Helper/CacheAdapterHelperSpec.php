<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\Helper;

use PhpSpec\ObjectBehavior;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;
use TorrentBundle\Exception\Configuration\BadCacheAdapterException;
use TorrentBundle\Helper\CacheAdapterHelper;
use TorrentBundle\Helper\HelperInterface;

class CacheAdapterHelperSpec extends ObjectBehavior
{
    public function let(
    ) {
        $this->beConstructedWith('memcached', '127.0.0.1', 11211);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(CacheAdapterHelper::class);
    }

    public function it_is_a_helper()
    {
        $this->shouldHaveType(HelperInterface::class);
    }

    public function it_tells_there_is_no_cache_adapter_available()
    {
        $this->isEmpty()->shouldReturn(false);
    }

    public function it_throws_an_exception_when_cache_client_is_not_available()
    {
        $this->beConstructedWith('unknown', '127.0.0.1', 11211);

        $this->shouldThrow(BadCacheAdapterException::class)->during('get');
        $this->getWhenAvailable()->shouldReturn(null);
    }

    public function it_throws_an_exception_when_memcached_is_not_available()
    {
        if (!MemcachedAdapter::isSupported()) {
            $this->shouldThrow(BadCacheAdapterException::class)->during('get');
            $this->getWhenAvailable()->shouldReturn(null);
        }
    }

    public function it_throws_an_exception_when_memcached_configuration_is_incorrect()
    {
        if (MemcachedAdapter::isSupported()) {
            $this->beConstructedWith('unknown', '127.0.0.1', 1);

            $this->shouldThrow(BadCacheAdapterException::class)->during('get');
            $this->getWhenAvailable()->shouldReturn(null);
        }
    }

    public function it_returns_filesystem_cache_adapter()
    {
        $this->beConstructedWith('filesystem', '', 0);

        $this->get()->shouldBeAnInstanceOf(FilesystemAdapter::class);
        $this->getWhenAvailable()->shouldBeAnInstanceOf(FilesystemAdapter::class);
    }
}
