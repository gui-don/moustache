<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\Adapter\Transmission\Mapper;

use PhpSpec\ObjectBehavior;
use StandardBundle\CanDownload;
use TorrentBundle\Adapter\Transmission\Mapper\TransmissionStatusMapper;
use TorrentBundle\Entity\TorrentInterface;
use Transmission\Model\Torrent;
use Transmission\Model\TrackerStats;

class TransmissionStatusMapperSpec extends ObjectBehavior
{
    public function let(
        TorrentInterface $torrent,
        Torrent $externalTorrent,
        TrackerStats $trackerStats1,
        TrackerStats $trackerStats2
    ) {
        $trackerStats1->getLastScrapeResult()->willReturn('A problem occured');
        $trackerStats2->getLastScrapeResult()->willReturn('');
        $trackerStats1->getHost()->willReturn('https://myHost.com:9999');
        $trackerStats2->getHost()->willReturn('https://supertorr.co.uk');

        $externalTorrent->getTrackerStats()->willReturn([$trackerStats1, $trackerStats2]);
        $externalTorrent->getStatus()->willReturn(3);

        $this->beConstructedWith();
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(TransmissionStatusMapper::class);
    }

    public function it_sets_status_to_external_status_when_all_trackers_are_ok($torrent, $externalTorrent, $trackerStats1)
    {
        $trackerStats1->getLastScrapeResult()->willReturn('');

        $torrent->setStatus(3)->shouldBeCalledTimes(1);
        $torrent->setStatusMessage('')->shouldBeCalledTimes(1);

        $this->mapStatus($torrent, $externalTorrent);
    }

    public function it_sets_status_to_external_status_when_at_least_one_tracker_is_ok($torrent, $externalTorrent)
    {
        $torrent->setStatus(3)->shouldBeCalledTimes(1);
        $torrent->setStatusMessage('myHost.com => “A problem occured”')->shouldBeCalledTimes(1);

        $this->mapStatus($torrent, $externalTorrent);
    }

    public function it_sets_failed_status_when_all_trackers_returned_errors($torrent, $externalTorrent, $trackerStats2)
    {
        $trackerStats2->getLastScrapeResult()->willReturn('Another problem');

        $torrent->setStatus(CanDownload::STATUS_FAIL)->shouldBeCalledTimes(1);
        $torrent->setStatusMessage('myHost.com => “A problem occured”; supertorr.co.uk => “Another problem”')->shouldBeCalledTimes(1);

        $this->mapStatus($torrent, $externalTorrent);
    }
}
