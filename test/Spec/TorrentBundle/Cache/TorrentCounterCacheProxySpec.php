<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\Cache;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Cache\TorrentCounterCacheProxy;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Entity\User;
use TorrentBundle\Helper\AuthenticatedUserHelper;

class TorrentCounterCacheProxySpec extends ObjectBehavior
{
    public function let(
        CacheInterface $cache,
        AuthenticatedUserHelper $authenticatedUserHelper,

        User $user
    ) {
        $user->getId()->willReturn(5);
        $authenticatedUserHelper->get()->willReturn($user);

        $this->beConstructedWith($cache, $authenticatedUserHelper);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(TorrentCounterCacheProxy::class);
    }

    public function it_is_a_cache()
    {
        $this->shouldHaveType(CacheInterface::class);
    }

    public function it_gets_a_torrent_by_its_hash($cache)
    {
        $cache->get(Argument::type('string'))->shouldBeCalledTimes(1)->willReturn('cache content');

        $this->get('key')->shouldReturn('cache content');
    }

    public function it_invalidates_a_torrent_by_its_hash($cache)
    {
        $cache->invalidate(Argument::type('string'))->shouldBeCalledTimes(1)->willReturn(true);

        $this->invalidate('key')->shouldReturn(true);
    }

    public function it_checks_torrent_status_by_its_hash($cache)
    {
        $cache->isUpToDate(Argument::type('string'))->shouldBeCalledTimes(1)->willReturn(false);

        $this->isUpToDate('key')->shouldReturn(false);
    }

    public function it_adds_a_torrent_to_cache_with_only_hash($cache, TorrentInterface $torrent)
    {
        $cache->set(Argument::type('string'), $torrent, CacheInterface::EXPIRATION_TORRENT_COUNT)->shouldBeCalledTimes(1);

        $this->set('key', $torrent);
    }
}
