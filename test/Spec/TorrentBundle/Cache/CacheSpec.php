<?php

declare(strict_types=1);

namespace Spec\TorrentBundle\Cache;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use TorrentBundle\Cache\Cache;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Helper\CacheAdapterHelper;

class CacheSpec extends ObjectBehavior
{
    public function let(
        CacheAdapterHelper $cacheAdapterHelper,

        AbstractAdapter $cacheAdapter,
        CacheItemInterface $cacheItem
    ) {
        $cacheItem->isHit()->willReturn(true);
        $cacheItem->get()->willReturn('cache content');
        $cacheAdapter->getItem(Argument::type('string'))->willReturn($cacheItem);

        $cacheAdapterHelper->get()->willReturn($cacheAdapter);

        $this->beConstructedWith($cacheAdapterHelper, 'client');
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(Cache::class);
    }

    public function it_is_a_cache($param)
    {
        $this->shouldHaveType(CacheInterface::class);
    }

    public function it_returns_a_cached_content()
    {
        $this->get('key')->shouldReturn('cache content');
    }

    public function it_invalidates_a_cached_content($cacheAdapter)
    {
        $cacheAdapter->deleteItem(Cache::CACHE_PREFIX.'.client.key')->shouldBeCalledTimes(1)->willReturn(true);

        $this->invalidate('key')->shouldReturn(true);
    }

    public function it_checks_if_cache_is_up_to_date()
    {
        $this->isUpToDate('key')->shouldReturn(true);
    }

    public function it_caches_a_value($cacheItem, $cacheAdapter)
    {
        $cacheItem->set('newValue')->shouldBeCalledTimes(1);
        $cacheItem->expiresAfter(40)->shouldBeCalledTimes(1);
        $cacheAdapter->save($cacheItem)->shouldBeCalledTimes(1);

        $this->set(CacheInterface::KEY_CLIENT_TOKEN, 'newValue', 40)->shouldReturn('newValue');
    }
}
