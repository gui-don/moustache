<?php

declare(strict_types=1);

namespace Spec\MoustacheBundle\EventListener;

use MoustacheBundle\EventListener\PaginationSetterListener;
use MoustacheBundle\Helper\PaginationHelper;
use MoustacheBundle\Helper\PaginationTotalPageHelper;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use TorrentBundle\Entity\UserInterface;
use TorrentBundle\Helper\AuthenticatedUserHelper;
use Twig_Environment;

class PaginationSetterListenerSpec extends ObjectBehavior
{
    public function let(
        PaginationHelper $paginationHelper,
        PaginationTotalPageHelper $paginationTotalPageHelper,
        Twig_Environment $twig,
        AuthenticatedUserHelper $authenticatedUserHelper,

        GetResponseEvent $event,
        UserInterface $authenticatedUser
    ) {
        $authenticatedUserHelper->getWhenAvailable()->willReturn($authenticatedUser);

        $paginationHelper->get()->willReturn(4);

        $paginationTotalPageHelper->get()->willReturn(6);

        $this->beConstructedWith($paginationHelper, $paginationTotalPageHelper, $twig, $authenticatedUserHelper);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(PaginationSetterListener::class);
    }

    public function it_does_nothing_if_there_is_not_any_authenticated_user($authenticatedUserHelper, $event)
    {
        $authenticatedUserHelper->getWhenAvailable()->willReturn(null);

        $this->onKernelRequest($event)->shouldReturn(null);
    }

    public function it_adds_pagination_values_as_twig_global_variables($twig, $event)
    {
        $twig->addGlobal('pagination', ['currentPage' => 4, 'totalPage' => 6])->shouldBeCalledTimes(1);

        $this->onKernelRequest($event)->shouldReturn($event);
    }
}
