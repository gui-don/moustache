<?php

declare(strict_types=1);

namespace Spec\MoustacheBundle\Helper;

use MoustacheBundle\Helper\PaginationHelper;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class PaginationHelperSpec extends ObjectBehavior
{
    public function let(
        RequestStack $requestStack,

        Request $request,
        ParameterBag $get,
        ParameterBag $post
    ) {
        $post->get(PaginationHelper::PAGINATION_PARAMETER)->willReturn(3);
        $get->get(PaginationHelper::PAGINATION_PARAMETER)->willReturn(null);

        $request->query = $get;
        $request->request = $post;

        $requestStack->getCurrentRequest()->willReturn($request);

        $this->beConstructedWith($requestStack);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(PaginationHelper::class);
    }

    public function it_returns_default_pagination_when_no_post_not_get_variables_are_present($post)
    {
        $post->get(PaginationHelper::PAGINATION_PARAMETER)->willReturn(null);

        $this->get()->shouldReturn(PaginationHelper::PAGINATION_DEFAULT_PAGE);
        $this->getWhenAvailable()->shouldReturn(PaginationHelper::PAGINATION_DEFAULT_PAGE);
    }

    public function it_returns_current_page_from_post_variables()
    {
        $this->get()->shouldReturn(3);
    }

    public function it_returns_current_page_from_get_variables($get)
    {
        $get->get(PaginationHelper::PAGINATION_PARAMETER)->willReturn(6);

        $this->get()->shouldReturn(6);
    }

    public function it_returns_current_item_position()
    {
        $this->getCurrentItems()->shouldReturn(3 * PaginationHelper::PAGINATION_MAX_RESULT);
    }

    public function it_tells_if_there_is_no_pagination()
    {
        $this->isEmpty()->shouldReturn(false);
    }
}
