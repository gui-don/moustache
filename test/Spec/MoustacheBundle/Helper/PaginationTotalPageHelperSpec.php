<?php

declare(strict_types=1);

namespace Spec\MoustacheBundle\Helper;

use MoustacheBundle\Helper\PaginationTotalPageHelper;
use PhpSpec\ObjectBehavior;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Filter\TorrentFilter;

class PaginationTotalPageHelperSpec extends ObjectBehavior
{
    public function let(CacheInterface $cache, TorrentFilter $torrentFilter)
    {
        $torrentFilter->getAllAuthenticatedUserTorrents()->willReturn([1, 2, 3, 4, 5, 6]);

        $this->beConstructedWith($cache, $torrentFilter);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(PaginationTotalPageHelper::class);
    }

    public function it_returns_the_total_pagination_pages()
    {
        $this->getWhenAvailable()->shouldReturn(0);
        $this->get()->shouldReturn(0);
    }

    public function it_tells_if_there_is_no_total_pagination_pages()
    {
        $this->isEmpty()->shouldReturn(false);
    }
}
