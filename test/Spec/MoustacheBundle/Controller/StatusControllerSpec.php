<?php

declare(strict_types=1);

namespace Spec\MoustacheBundle\Controller;

use MoustacheBundle\Controller\StatusController;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TorrentBundle\Client\ClientInterface;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Exception\Client\TorrentAdapterException;
use TorrentBundle\Exception\Torrent\TorrentNotFoundException;

class StatusControllerSpec extends ObjectBehavior
{
    public function let(
        ClientInterface $torrentClient,

        TorrentInterface $torrent1,
        TorrentInterface $torrent2
    ) {
        $torrentClient->getPaginated()->willReturn([$torrent1, $torrent2]);
        $torrentClient->get(3)->willReturn($torrent1);

        $this->beConstructedWith($torrentClient);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(StatusController::class);
    }

    public function it_gets_the_status_of_a_all_torrent_within_current_pagination()
    {
        $this->getAllAction()->shouldHaveType(JsonResponse::class);
    }

    public function it_gets_the_status_of_a_single_torrent()
    {
        $this->getAction(3)->shouldHaveType(JsonResponse::class);
    }

    public function it_throws_a_not_found_exception_when_adapter_exception_is_catched($torrentClient)
    {
        $torrentClient->get(3)->willThrow(TorrentAdapterException::class);

        $this->shouldThrow(NotFoundHttpException::class)->during('getAction', [3]);
    }

    public function it_throws_a_not_found_exception_when_torrent_not_found_is_catched($torrentClient)
    {
        $torrentClient->get(3)->willThrow(TorrentNotFoundException::class);

        $this->shouldThrow(NotFoundHttpException::class)->during('getAction', [3]);
    }
}
