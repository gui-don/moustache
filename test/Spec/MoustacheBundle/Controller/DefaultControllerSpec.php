<?php

declare(strict_types=1);

namespace Spec\MoustacheBundle\Controller;

use MoustacheBundle\Controller\DefaultController;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use TorrentBundle\Client\ClientInterface;
use TorrentBundle\Entity\TorrentInterface;

class DefaultControllerSpec extends ObjectBehavior
{
    public function let(
        ClientInterface $torrentClient,
        EngineInterface $templateEngine,
        FormInterface $torrentMenuForm,

        TorrentInterface $torrent1,
        TorrentInterface $torrent2,
        Response $response
    ) {
        $torrentClient->getPaginated()->willReturn([$torrent1, $torrent2]);
        $torrentClient->get(3)->willReturn($torrent1);

        $torrentMenuForm->setData(Argument::type(TorrentInterface::class))->willReturn($torrentMenuForm);
        $torrentMenuForm->createView()->willReturn('view');

        $templateEngine->renderResponse(Argument::type('string'), Argument::type('array'))->willReturn($response);

        $this->beConstructedWith($torrentClient, $templateEngine, $torrentMenuForm);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(DefaultController::class);
    }

    public function it_lists_all_the_torrents($response)
    {
        $this->listAction()->shouldReturn($response);
    }

    public function it_show_a_torrent_content($response)
    {
        $this->torrentContentAction(3)->shouldReturn($response);
    }
}
