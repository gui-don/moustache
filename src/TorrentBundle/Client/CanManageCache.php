<?php

declare(strict_types=1);

namespace TorrentBundle\Client;

interface CanManageCache
{
    /**
     * Can be essential for some RPC client (like transmission) who have dynamic ids.
     */
    public function updateCache();
}
