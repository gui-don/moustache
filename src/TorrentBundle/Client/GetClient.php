<?php

declare(strict_types=1);

namespace TorrentBundle\Client;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TorrentBundle\Adapter\AdapterInterface;
use TorrentBundle\Adapter\TorrentMapperInterface;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Event\Events;
use TorrentBundle\Exception\Torrent\TorrentNotFoundException;
use TorrentBundle\Filter\TorrentFilter;

class GetClient implements GetClientInterface
{
    use MapperTrait;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var AdapterInterface
     */
    private $externalClient;

    /**
     * @var TorrentMapperInterface
     */
    private $torrentMapper;

    /**
     * @var TorrentFilter
     */
    private $torrentFilter;

    /**
     * @var ExternalTorrentGetter
     */
    private $externalTorrentGetter;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @param AdapterInterface         $externalClient
     * @param TorrentMapperInterface   $torrentMapper
     * @param TorrentFilter            $torrentFilter
     * @param ExternalTorrentGetter    $externalTorrentGetter
     * @param CacheInterface           $cache
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, AdapterInterface $externalClient, TorrentMapperInterface $torrentMapper, TorrentFilter $torrentFilter, ExternalTorrentGetter $externalTorrentGetter, CacheInterface $cache)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->externalClient = $externalClient;
        $this->torrentMapper = $torrentMapper;
        $this->torrentFilter = $torrentFilter;
        $this->externalTorrentGetter = $externalTorrentGetter;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     *
     * @throws TorrentNotFoundException
     **/
    public function get(int $id): TorrentInterface
    {
        $notMappedTorrent = $this->getAuthenticatedUserTorrent($id);

        if ($this->isTorrentInCache($notMappedTorrent->getHash())) {
            return $this->cache->get($notMappedTorrent->getHash());
        }

        return $this->mapAndDispatchEvent($notMappedTorrent, $this->externalTorrentGetter->get($notMappedTorrent->getHash()), Events::AFTER_TORRENT_GET);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaginated(): array
    {
        return $this->getAuthenticatedUserTorrentList($this->torrentFilter->getPaginatedAuthenticatedUserTorrents());
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(): array
    {
        return $this->getAuthenticatedUserTorrentList($this->torrentFilter->getAllAuthenticatedUserTorrents());
    }

    public function getAuthenticatedUserTorrentList(array $notMappedTorrents): array
    {
        return array_filter(array_map(function ($notMappedTorrent) {
            if ($this->isTorrentInCache($notMappedTorrent->getHash())) {
                return $this->cache->get($notMappedTorrent->getHash());
            }

            $externalTorrent = $this->externalTorrentGetter->get($notMappedTorrent->getHash());
            if (null !== $externalTorrent) {
                return $this->mapAndDispatchEvent($notMappedTorrent, $externalTorrent, Events::AFTER_TORRENT_GET);
            }
        }, $notMappedTorrents));
    }

    private function getAuthenticatedUserTorrent(int $id): TorrentInterface
    {
        $notMappedTorrent = $this->torrentFilter->getAuthenticatedUserTorrent($id);

        if (null === $notMappedTorrent) {
            throw new TorrentNotFoundException($id);
        }

        return $notMappedTorrent;
    }

    private function isTorrentInCache(string $hash): bool
    {
        return
            $this->cache->isUpToDate($hash) &&
            $this->cache->get($hash) instanceof TorrentInterface
        ;
    }
}
