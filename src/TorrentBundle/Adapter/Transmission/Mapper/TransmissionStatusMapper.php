<?php

declare(strict_types=1);

namespace TorrentBundle\Adapter\Transmission\Mapper;

use StandardBundle\CanDownload;
use TorrentBundle\Entity\TorrentInterface;
use Transmission\Model\Torrent as ExternalTorrent;

class TransmissionStatusMapper
{
    /**
     * @param TorrentInterface $torrent
     * @param ExternalTorrent  $externalTorrent
     *
     * @return TorrentInterface
     */
    public function mapStatus(TorrentInterface $torrent, ExternalTorrent $externalTorrent): TorrentInterface
    {
        $statusMessages = array_filter(array_unique(array_map(function ($trackerStats) {
            if ('' === $trackerStats->getLastScrapeResult()) {
                return null;
            }

            return sprintf('%s => “%s”', $this->extractHost($trackerStats->getHost()), $trackerStats->getLastScrapeResult());
        }, $externalTorrent->getTrackerStats())));

        if ($this->allTrackersFailed($externalTorrent->getTrackerStats(), $statusMessages)) {
            $torrent->setStatus(CanDownload::STATUS_FAIL);
        } else {
            $torrent->setStatus($externalTorrent->getStatus());
        }

        $torrent->setStatusMessage(implode('; ', $statusMessages));

        return $torrent;
    }

    private function allTrackersFailed(array $trackerStats, array $statusMessages)
    {
        return
            !empty($statusMessages) &&
            count($statusMessages) === count($trackerStats)
        ;
    }

    private function extractHost(string $fullUrl): string
    {
        return parse_url($fullUrl, PHP_URL_HOST);
    }
}
