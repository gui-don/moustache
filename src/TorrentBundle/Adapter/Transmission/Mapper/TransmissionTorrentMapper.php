<?php

declare(strict_types=1);

namespace TorrentBundle\Adapter\Transmission\Mapper;

use DateTime;
use Rico\Lib\StringUtils;
use TorrentBundle\Adapter\FileMapperInterface;
use TorrentBundle\Adapter\FriendlyNameTrait;
use TorrentBundle\Adapter\TorrentMapperInterface;
use TorrentBundle\Entity\File;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Service\MimeGuesser;

class TransmissionTorrentMapper implements TorrentMapperInterface
{
    use FriendlyNameTrait;

    /**
     * @var TransmissionStatusMapper
     */
    private $transmissionStatusMapper;

    /**
     * @var StringUtils
     */
    private $stringUtils;

    /**
     * @var MimeGuesser
     */
    private $mimeGuesser;

    /**
     * @var FileMapperInterface
     */
    private $fileMapper;

    /**
     * @param TransmissionStatusMapper $transmissionStatusMapper
     * @param StringUtils              $stringUtils
     * @param MimeGuesser              $mimeGuesser
     * @param FileMapperInterface      $fileMapper
     */
    public function __construct(TransmissionStatusMapper $transmissionStatusMapper, StringUtils $stringUtils, MimeGuesser $mimeGuesser, FileMapperInterface $fileMapper)
    {
        $this->transmissionStatusMapper = $transmissionStatusMapper;
        $this->stringUtils = $stringUtils;
        $this->mimeGuesser = $mimeGuesser;
        $this->fileMapper = $fileMapper;
    }

    /**
     * {@inheritdoc}
     */
    public function map(TorrentInterface $torrent, $externalTorrent): TorrentInterface
    {
        $torrent->setHash($externalTorrent->getHash());
        $torrent->setUploadRate($externalTorrent->getUploadRate());
        $torrent->setDownloadRate($externalTorrent->getDownloadRate());
        $torrent->setStatus($externalTorrent->getStatus());
        $torrent->setStartDate($externalTorrent->getStartDate() ? new DateTime('@'.$externalTorrent->getStartDate()) : new DateTime());
        $torrent->setCurrentByteSize((int) ($externalTorrent->getSize() * $externalTorrent->getPercentDone() / 100));
        $torrent->setTotalByteSize($externalTorrent->getSize());
        $torrent->setFriendlyName($this->getFriendlyName($externalTorrent->getName()));
        $torrent->setName($externalTorrent->getName());
        $torrent->setNbPeers(count($externalTorrent->getPeers()));
        $torrent->setDownloadDir($externalTorrent->getDownloadDir() ?? '');

        $torrent = $this->transmissionStatusMapper->mapStatus($torrent, $externalTorrent);
        $this->mapMime($torrent, $externalTorrent);

        $torrent->setFiles([]);

        return $torrent;
    }

    /**
     * {@inheritdoc}
     */
    public function mapFiles(TorrentInterface $torrent, $externalTorrent): TorrentInterface
    {
        $files = array_map(function ($externalFile) use ($torrent) {
            return $this->fileMapper->map(new File(), $externalFile, $torrent);
        }, $externalTorrent->getFiles());

        $torrent->setFiles($files);

        return $torrent;
    }

    private function mapMime(TorrentInterface &$torrent, $externalTorrent)
    {
        if (1 === count($externalTorrent->getFiles())) {
            $torrent->setMime($this->mimeGuesser->guessMimeByFilename($externalTorrent->getName()));
        } else {
            $torrent->setMime(MimeGuesser::MIME_DIRECTORY);
        }
    }
}
