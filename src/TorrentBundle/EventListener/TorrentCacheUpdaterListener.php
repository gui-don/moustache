<?php

declare(strict_types=1);

namespace TorrentBundle\EventListener;

use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Event\TorrentAfterEvent;

/**
 * Fills the outdated cache (of a torrent) with fresh data.
 */
final class TorrentCacheUpdaterListener
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param TorrentAfterEvent $event
     *
     * @return TorrentAfterEvent
     */
    public function afterTorrentUpdated(TorrentAfterEvent $event): TorrentAfterEvent
    {
        $this->cache->set($event->getTorrent()->getHash(), $event->getTorrent());

        return $event;
    }

    /**
     * @param TorrentAfterEvent $event
     *
     * @return TorrentAfterEvent
     */
    public function afterTorrentRemoved(TorrentAfterEvent $event): TorrentAfterEvent
    {
        $this->cache->invalidate($event->getTorrent()->getHash());

        return $event;
    }
}
