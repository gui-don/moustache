<?php

declare(strict_types=1);

namespace TorrentBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Event\ClientAfterEvent;
use TorrentBundle\Event\TorrentAfterEvent;
use TorrentBundle\Exception\Client\CacheOutdatedException;
use TorrentBundle\Helper\TorrentClientHelper;

/**
 * Fills the outdated cache (torrent hashes) with fresh data.
 */
class TorrentHashCacheUpdaterListener
{
    /**
     * @var TorrentClientHelper
     */
    private $torrentClientHelper;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param TorrentClientHelper $torrentClientHelper
     * @param CacheInterface      $cache
     * @param LoggerInterface     $logger
     */
    public function __construct(TorrentClientHelper $torrentClientHelper, CacheInterface $cache, LoggerInterface $logger)
    {
        $this->torrentClientHelper = $torrentClientHelper;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @param ClientAfterEvent $event
     *
     * @return ClientAfterEvent
     */
    public function afterClientRetrieved(ClientAfterEvent $event): ClientAfterEvent
    {
        if (!$this->shouldUpdate()) {
            return $event;
        }

        $this->doUpdateCache('Cache updated for client “%s” after initialization.');

        return $event;
    }

    /**
     * @param TorrentAfterEvent $event
     *
     * @return TorrentAfterEvent
     */
    public function afterTorrentAdded(TorrentAfterEvent $event): TorrentAfterEvent
    {
        $this->doUpdateCache('Cache updated for client “%s” after adding a new torrent.');

        return $event;
    }

    /**
     * @param TorrentAfterEvent $event
     *
     * @return TorrentAfterEvent
     */
    public function afterTorrentRemoved(TorrentAfterEvent $event): TorrentAfterEvent
    {
        $this->doUpdateCache('Cache updated for client “%s” after removing a torrent.');

        return $event;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @return GetResponseForExceptionEvent
     */
    public function onKernelException(GetResponseForExceptionEvent $event): GetResponseForExceptionEvent
    {
        if (!$event->getException() instanceof CacheOutdatedException) {
            return $event;
        }

        $this->doUpdateCache('Cache updated for client “%s” after an exception occured.');

        return $event;
    }

    private function doUpdateCache(string $logMessage)
    {
        $this->torrentClientHelper->get()->updateCache();
        $this->logger->info(sprintf($logMessage, $this->torrentClientHelper->get()->getName()));
    }

    private function shouldUpdate(): bool
    {
        return
            !$this->torrentClientHelper->isEmpty() &&
            !$this->cache->isUpToDate(CacheInterface::KEY_TORRENT_HASHES)
        ;
    }
}
