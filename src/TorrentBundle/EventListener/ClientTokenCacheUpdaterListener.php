<?php

declare(strict_types=1);

namespace TorrentBundle\EventListener;

use Psr\Log\LoggerInterface;
use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Event\ClientAfterEvent;

/**
 * Fills cache with client token after a client is retrieved.
 */
class ClientTokenCacheUpdaterListener
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CacheInterface  $cache
     * @param LoggerInterface $logger
     */
    public function __construct(CacheInterface $cache, LoggerInterface $logger)
    {
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @param ClientAfterEvent $event
     *
     * @return ClientAfterEvent
     */
    public function afterClientRetrieved(ClientAfterEvent $event): ClientAfterEvent
    {
        if (!$this->shouldCache()) {
            return $event;
        }

        $this->saveClientToken($event->getClient()->getSessionToken());

        $this->logger->info(sprintf('Cache for client “%s” was initialized.', $event->getClient()->getName()));

        return $event;
    }

    private function saveClientToken(string $clientToken)
    {
        $this->cache->set(CacheInterface::KEY_CLIENT_TOKEN, $clientToken, CacheInterface::EXPIRATION_CLIENT_TOKEN);
    }

    private function shouldCache(): bool
    {
        return
            !$this->cache->isUpToDate(CacheInterface::KEY_CLIENT_TOKEN)
        ;
    }
}
