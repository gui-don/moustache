<?php

declare(strict_types=1);

namespace TorrentBundle\EventListener;

use Symfony\Component\Filesystem\Filesystem;
use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Event\TorrentAfterEvent;

/**
 * Removes a temporary file after it’s been used.
 */
final class UploadedFileRemoverListener
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param TorrentAfterEvent $event
     *
     * @return TorrentAfterEvent|null
     */
    public function afterTorrentAdded(TorrentAfterEvent $event)
    {
        if (!$this->isTorrentFileFindable($event->getTorrent())) {
            return null;
        }

        $this->filesystem->remove($event->getTorrent()->getUploadedFile()->getRealPath());
        $event->getTorrent()->setUploadedFile(null);
        $event->getTorrent()->setUploadedFileByUrl(null);

        return $event;
    }

    private function isTorrentFileFindable(TorrentInterface $torrent): bool
    {
        return
            $torrent->getUploadedFile() &&
            $torrent->getUploadedFile()->getRealPath()
        ;
    }
}
