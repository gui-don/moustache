<?php

declare(strict_types=1);

namespace TorrentBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MagnetLink extends Constraint
{
    private $message = 'The input link does not seem to be a valid magnet';
}
