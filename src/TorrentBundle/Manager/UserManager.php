<?php

declare(strict_types=1);

namespace TorrentBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Doctrine\UserManager as FosUserManager;
use FOS\UserBundle\Model\UserInterface as FosUserInterface;
use TorrentBundle\Entity\UserInterface;

class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FosUserManager
     */
    private $fosUserManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FosUserManager         $fosUserManager
     */
    public function __construct(EntityManagerInterface $entityManager, FosUserManager $fosUserManager)
    {
        $this->entityManager = $entityManager;
        $this->fosUserManager = $fosUserManager;
    }

    /**
     * @param UserInterface $user
     */
    public function incrementCurrentMessage(UserInterface $user)
    {
        $user->setCurrentMessage($user->getCurrentMessage() + 1);

        $this->entityManager->persist($user);
    }

    /**
     * @return FosUserInterface
     */
    public function create(string $username): FosUserInterface
    {
        $user = $this->fosUserManager->createUser();
        $user->setUsername($username);
        $user->setEmail($username);
        $user->setEnabled(false);
        $user->setPlainPassword(random_bytes(20));

        $this->update($user);

        return $user;
    }

    /**
     * @param FosUserInterface $user
     */
    public function update(FosUserInterface $user)
    {
        $this->fosUserManager->updateUser($user);
    }

    /**
     * @param FosUserInterface $user
     */
    public function generateConfirmationToken(FosUserInterface $user)
    {
        $user->setConfirmationToken(hash('sha256', random_bytes(15)));

        $this->entityManager->persist($user);
    }

    public function flush()
    {
        $this->entityManager->flush();
    }
}
