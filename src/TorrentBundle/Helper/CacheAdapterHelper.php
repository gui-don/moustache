<?php

declare(strict_types=1);

namespace TorrentBundle\Helper;

use Memcached;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use TorrentBundle\Exception\Configuration\BadCacheAdapterException;

class CacheAdapterHelper implements HelperInterface
{
    const AVAILABLE_CACHE_ADAPTERS = [
        self::ADAPTER_REDIS,
        self::ADAPTER_MEMCACHED,
        self::ADAPTER_FILESYSTEM,
    ];

    const ADAPTER_REDIS = 'redis';
    const ADAPTER_MEMCACHED = 'memcached';
    const ADAPTER_FILESYSTEM = 'filesystem';

    /**
     * @var string
     */
    private $cacheName;

    /**
     * @var string
     */
    private $cacheHost;

    /**
     * @var int
     */
    private $cachePort;

    /**
     * @param string $cacheName
     * @param string $cacheHost
     * @param int    $cachePort
     */
    public function __construct(string $cacheName, string $cacheHost, int $cachePort)
    {
        $this->cacheName = $cacheName;
        $this->cacheHost = $cacheHost;
        $this->cachePort = $cachePort;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @return AbstractAdapter|null
     */
    public function getWhenAvailable()
    {
        try {
            return $this->get();
        } catch (BadCacheAdapterException $ex) {
            return null;
        }
    }

    /**
     * @throws BadCacheAdapterException
     *
     * @return AbstractAdapter
     */
    public function get(): AbstractAdapter
    {
        $this->checkAvailability();

        return $this->tryRedis() ?? $this->tryMemcached() ?? new FilesystemAdapter();
    }

    private function tryRedis()
    {
        if (self::ADAPTER_REDIS === $this->cacheName) {
            try {
                $redisClient = RedisAdapter::createConnection(sprintf('redis://%s:%s', $this->cacheHost, $this->cachePort));

                return new RedisAdapter($redisClient);
            } catch (\Exception $ex) {
                throw new BadCacheAdapterException(sprintf('Redis caching system is badly configured. Exception: “%s”', $ex->getMessage()), 0, $ex);
            }
        }
    }

    private function tryMemcached()
    {
        if (self::ADAPTER_MEMCACHED === $this->cacheName) {
            $this->checkMemcachedAvailability();

            $memcachedClient = MemcachedAdapter::createConnection(sprintf('memcached://%s:%s', $this->cacheHost, $this->cachePort));
            $this->checkMemcachedConfiguration($memcachedClient);

            return new MemcachedAdapter($memcachedClient);
        }
    }

    private function checkAvailability()
    {
        if (!in_array($this->cacheName, self::AVAILABLE_CACHE_ADAPTERS, true)) {
            throw new BadCacheAdapterException(
                sprintf('No cache adapter named “%s” is available. Available: “%s”.', $this->cacheName, implode(', ', self::AVAILABLE_CACHE_ADAPTERS))
            );
        }
    }

    private function checkMemcachedAvailability()
    {
        if (!MemcachedAdapter::isSupported()) {
            throw new BadCacheAdapterException('Memcached caching system was configured but it does not seemed installed on the system.');
        }
    }

    private function checkMemcachedConfiguration(Memcached $memcachedClient)
    {
        $memcachedClient->add('test_key', 'test_value');
        $result = $memcachedClient->getResultCode();

        if (Memcached::RES_SUCCESS !== $result && Memcached::RES_DATA_EXISTS !== $result) {
            throw new BadCacheAdapterException(
                sprintf(
                    'Memcached caching system is badly configured. Memcached server returned code “%s” (check meaning at “ %s ”)',
                    $result,
                    'https://secure.php.net/manual/fr/memcached.getresultcode.php'
                )
            );
        }
    }
}
