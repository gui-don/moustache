<?php

declare(strict_types=1);

namespace TorrentBundle\Cache;

use TorrentBundle\Entity\TorrentInterface;

class TorrentCacheProxy implements CacheInterface
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $hash
     *
     * @return TorrentInterface|null
     */
    public function get(string $hash)
    {
        return $this->cache->get($this->getTorrentCacheKey($hash));
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function invalidate(string $hash): bool
    {
        return $this->cache->invalidate($this->getTorrentCacheKey($hash));
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function isUpToDate(string $hash): bool
    {
        return $this->cache->isUpToDate($this->getTorrentCacheKey($hash));
    }

    /**
     * @param string $hash
     * @param mixed  $value
     * @param int    $expiration
     */
    public function set(string $hash, $value, int $expiration = CacheInterface::EXPIRATION_TORRENT_LIST)
    {
        $this->cache->set($this->getTorrentCacheKey($hash), $value, $expiration);
    }

    private function getTorrentCacheKey(string $hash): string
    {
        return CacheInterface::KEY_TORRENT_LIST.'.'.$hash;
    }
}
