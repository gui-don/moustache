<?php

declare(strict_types=1);

namespace TorrentBundle\Cache;

use TorrentBundle\Helper\AuthenticatedUserHelper;

class TorrentCounterCacheProxy implements CacheInterface
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var AuthenticatedUserHelper
     */
    private $authenticatedUserHelper;

    /**
     * @param CacheInterface          $cache
     * @param AuthenticatedUserHelper $authenticatedUserHelper
     */
    public function __construct(CacheInterface $cache, AuthenticatedUserHelper $authenticatedUserHelper)
    {
        $this->cache = $cache;
        $this->authenticatedUserHelper = $authenticatedUserHelper;
    }

    /**
     * @param string $type
     *
     * @return int|null
     */
    public function get(string $type)
    {
        return $this->cache->get($this->getTorrentCountCacheKey($type));
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function invalidate(string $hash): bool
    {
        return $this->cache->invalidate($this->getTorrentCountCacheKey($hash));
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function isUpToDate(string $hash): bool
    {
        return $this->cache->isUpToDate($this->getTorrentCountCacheKey($hash));
    }

    /**
     * @param string $hash
     * @param mixed  $value
     * @param int    $expiration
     */
    public function set(string $hash, $value, int $expiration = CacheInterface::EXPIRATION_TORRENT_COUNT)
    {
        $this->cache->set($this->getTorrentCountCacheKey($hash), $value, $expiration);
    }

    private function getTorrentCountCacheKey(string $type): string
    {
        return sprintf(CacheInterface::KEY_TORRENT_COUNT, $this->authenticatedUserHelper->get()->getId(), $type);
    }
}
