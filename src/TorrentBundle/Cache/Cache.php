<?php

declare(strict_types=1);

namespace TorrentBundle\Cache;

use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use TorrentBundle\Helper\CacheAdapterHelper;

class Cache implements CacheInterface
{
    const CACHE_PREFIX = 'moustache';

    /**
     * @var CacheAdapterHelper
     */
    private $cacheAdapterHelper;

    /**
     * @var string
     */
    private $rpcClient;

    /**
     * @var AbstractAdapter
     */
    private $cacheAdapter;

    /**
     * @param CacheAdapterHelper $cacheAdapterHelper
     * @param string             $rpcClient
     */
    public function __construct(CacheAdapterHelper $cacheAdapterHelper, string $rpcClient)
    {
        $this->cacheAdapterHelper = $cacheAdapterHelper;
        $this->cacheAdapter = $cacheAdapterHelper->get();
        $this->rpcClient = $rpcClient;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        return $this->getCacheItem($key)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function invalidate(string $key): bool
    {
        return $this->cacheAdapter->deleteItem($this->getFullKey($key));
    }

    /**
     * {@inheritdoc}
     */
    public function isUpToDate(string $key): bool
    {
        return $this->getCacheItem($key)->isHit();
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, $value, int $expiration = self::EXPIRATION_DEFAULT)
    {
        $cacheItem = $this->getCacheItem($key);

        $cacheItem->set($value);
        $cacheItem->expiresAfter($expiration);

        $this->cacheAdapter->save($cacheItem);

        return $value;
    }

    public function getFullKey(string $key): string
    {
        return self::CACHE_PREFIX.'.'.$this->rpcClient.'.'.$key;
    }

    private function getCacheItem(string $key): CacheItemInterface
    {
        return $this->cacheAdapter->getItem($this->getFullKey($key));
    }
}
