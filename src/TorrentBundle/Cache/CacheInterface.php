<?php

declare(strict_types=1);

namespace TorrentBundle\Cache;

interface CacheInterface
{
    const EXPIRATION_DEFAULT = 3600;
    const EXPIRATION_CLIENT_TOKEN = 86400;
    const EXPIRATION_TORRENT_HASHES = 30;
    const EXPIRATION_TORRENT_LIST = 2;
    const EXPIRATION_TORRENT_COUNT = 10;

    const KEY_CLIENT_TOKEN = 'client.token';
    const KEY_TORRENT_HASHES = 'torrent_hashes';
    const KEY_TORRENT_LIST = 'torrent_list';
    const KEY_TORRENT_COUNT = '%s.torrent_count.%s';

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key);

    /**
     * @param string $key
     *
     * @return bool
     */
    public function invalidate(string $key): bool;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function isUpToDate(string $key): bool;

    /**
     * @param string $key
     * @param mixed  $value
     * @param int    $expiration
     *
     * @return mixed
     */
    public function set(string $key, $value, int $expiration = self::EXPIRATION_DEFAULT);
}
