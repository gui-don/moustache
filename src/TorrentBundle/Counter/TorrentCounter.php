<?php

declare(strict_types=1);

namespace TorrentBundle\Counter;

use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Client\ClientInterface;
use TorrentBundle\Entity\TorrentInterface;

class TorrentCounter
{
    const CACHE_KEY_FAILED = 'failed';
    const CACHE_KEY_STARTED = 'started';
    const CACHE_KEY_STOPPED = 'stopped';
    const CACHE_KEY_TOTAL = 'total';

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var ClientInterface
     */
    private $torrentClient;

    /**
     * @param CacheInterface  $cache
     * @param ClientInterface $torrentClient
     */
    public function __construct(CacheInterface $cache, ClientInterface $torrentClient)
    {
        $this->cache = $cache;
        $this->torrentClient = $torrentClient;
    }

    public function getFailed(): int
    {
        return $this->getCacheValue(self::CACHE_KEY_FAILED) ?? $this->updateFailed();
    }

    public function getStarted(): int
    {
        return $this->getCacheValue(self::CACHE_KEY_STARTED) ?? $this->updateStarted();
    }

    public function getStopped(): int
    {
        return $this->getCacheValue(self::CACHE_KEY_STOPPED) ?? $this->updateStopped();
    }

    public function getTotal(): int
    {
        return $this->getCacheValue(self::CACHE_KEY_TOTAL) ?? $this->updateTotal();
    }

    public function updateFailed(): int
    {
        return $this->setCacheValue(self::CACHE_KEY_FAILED, count(array_filter($this->torrentClient->getAll(), function (TorrentInterface $torrent) {
            return $torrent->isFailed();
        })));
    }

    public function updateStarted(): int
    {
        return $this->setCacheValue(self::CACHE_KEY_STARTED, count(array_filter($this->torrentClient->getAll(), function (TorrentInterface $torrent) {
            return $torrent->isStarted();
        })));
    }

    public function updateStopped(): int
    {
        return $this->setCacheValue(self::CACHE_KEY_STOPPED, count(array_filter($this->torrentClient->getAll(), function (TorrentInterface $torrent) {
            return $torrent->isStopped();
        })));
    }

    public function updateTotal(): int
    {
        return $this->setCacheValue(self::CACHE_KEY_TOTAL, count($this->torrentClient->getAll()));
    }

    private function setCacheValue(string $type, int $value): int
    {
        return $this->cache->set($type, $value);
    }

    /**
     * @param string $type
     *
     * @return int|null
     */
    private function getCacheValue(string $type)
    {
        if ($this->isCacheHit($type)) {
            return $this->cache->get($type);
        }
    }

    private function isCacheHit(string $type): bool
    {
        return $this->cache->isUpToDate($type);
    }
}
