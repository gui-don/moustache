<?php

declare(strict_types=1);

namespace TorrentBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use MoustacheBundle\Helper\PaginationHelper;
use TorrentBundle\Entity\Torrent;

class TorrentRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PaginationHelper
     */
    private $paginationHelper;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PaginationHelper       $paginationHelper
     */
    public function __construct(EntityManagerInterface $entityManager, PaginationHelper $paginationHelper)
    {
        $this->entityManager = $entityManager;
        $this->paginationHelper = $paginationHelper;
    }

    /**
     * @param int $userId
     *
     * @return Torrent[]
     */
    public function findAllTorrentsByUser(int $userId): array
    {
        return $this
            ->createQueryBuilder('t')
            ->select('t')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int $userId
     *
     * @return Torrent[]
     */
    public function findPaginatedTorrentsByUser(int $userId): array
    {
        return $this
            ->createQueryBuilder('t')
            ->select('t')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId)
            ->setMaxResults(PaginationHelper::PAGINATION_MAX_RESULT)
            ->setFirstResult($this->paginationHelper->getCurrentItems())
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int $userId
     * @param int $torrentId
     *
     * @return Torrent|null
     */
    public function findOneByUserAndId(int $userId, int $torrentId)
    {
        return $this
            ->createQueryBuilder('t')
            ->select('t')
            ->andWhere('t.id = :torrentId')
            ->andWhere('t.user = :userId')
            ->setParameter('torrentId', $torrentId)
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    private function createQueryBuilder(string $alias): QueryBuilder
    {
        return $this->entityManager->createQueryBuilder()->from(Torrent::class, $alias);
    }
}
