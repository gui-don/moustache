<?php

declare(strict_types=1);

namespace TorrentBundle\Entity;

use Rico\Slib\StringUtils;
use StandardBundle\CanDownload;

trait CanDownloadTrait
{
    /**
     * @var int
     */
    protected $downloadRate = 0;

    /**
     * @var int
     */
    protected $status = CanDownload::STATUS_STOP;

    /**
     * @var string
     */
    protected $statusMessage = '';

    /**
     * {@inheritdoc}
     */
    public function getDownloadHumanRate(): string
    {
        return StringUtils::humanFilesize($this->downloadRate);
    }

    /**
     * {@inheritdoc}
     */
    public function isDownloading(): bool
    {
        return $this->downloadRate > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function isStarted(): bool
    {
        return !$this->isStopped();
    }

    /**
     * {@inheritdoc}
     */
    public function isStopped(): bool
    {
        return
            CanDownload::STATUS_DOWNLOADING !== $this->status &&
            CanDownload::STATUS_DONE !== $this->status
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function isFailed(): bool
    {
        return
            CanDownload::STATUS_FAIL === $this->status
        ;
    }

    // ---

    /**
     * {@inheritdoc}
     */
    public function getDownloadRate(): int
    {
        return $this->downloadRate;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusMessage(): string
    {
        return $this->statusMessage;
    }

    // ---

    /**
     * {@inheritdoc}
     */
    public function setDownloadRate(int $downloadRate = null)
    {
        $this->downloadRate = $downloadRate ?? 0;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatusMessage(string $statusMessage)
    {
        $this->statusMessage = $statusMessage;
    }
}
