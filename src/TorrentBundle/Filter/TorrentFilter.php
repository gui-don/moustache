<?php

declare(strict_types=1);

namespace TorrentBundle\Filter;

use TorrentBundle\Entity\TorrentInterface;
use TorrentBundle\Helper\AuthenticatedUserHelper;
use TorrentBundle\Repository\TorrentRepository;

class TorrentFilter
{
    /**
     * @var AuthenticatedUserHelper
     */
    private $authenticatedUserHelper;

    /**
     * @var TorrentRepository
     */
    private $torrentRepository;

    /**
     * @param AuthenticatedUserHelper $authenticatedUserHelper
     * @param TorrentRepository       $torrentRepository
     */
    public function __construct(AuthenticatedUserHelper $authenticatedUserHelper, TorrentRepository $torrentRepository)
    {
        $this->authenticatedUserHelper = $authenticatedUserHelper;
        $this->torrentRepository = $torrentRepository;
    }

    /**
     * @return TorrentInterface[]
     */
    public function getAllAuthenticatedUserTorrents(): array
    {
        $authenticatedUser = $this->authenticatedUserHelper->get();

        return $this->torrentRepository->findAllTorrentsByUser($authenticatedUser->getId());
    }

    /**
     * @return TorrentInterface[]
     */
    public function getPaginatedAuthenticatedUserTorrents(): array
    {
        $authenticatedUser = $this->authenticatedUserHelper->get();

        return $this->torrentRepository->findPaginatedTorrentsByUser($authenticatedUser->getId());
    }

    /**
     * @param int $torrentId
     *
     * @return TorrentInterface|null
     */
    public function getAuthenticatedUserTorrent(int $torrentId)
    {
        $authenticatedUser = $this->authenticatedUserHelper->get();

        return $this->torrentRepository->findOneByUserAndId($authenticatedUser->getId(), $torrentId);
    }
}
