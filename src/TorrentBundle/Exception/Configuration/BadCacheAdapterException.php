<?php

declare(strict_types=1);

namespace TorrentBundle\Exception\Configuration;

class BadCacheAdapterException extends ConfigurationException
{
}
