<?php

declare(strict_types=1);

namespace MoustacheBundle\Helper;

use TorrentBundle\Cache\CacheInterface;
use TorrentBundle\Filter\TorrentFilter;
use TorrentBundle\Helper\HelperInterface;

class PaginationTotalPageHelper implements HelperInterface
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var TorrentFilter
     */
    private $torrentFilter;

    /**
     * @param CacheInterface $cache
     * @param TorrentFilter  $torrentFilter
     */
    public function __construct(CacheInterface $cache, TorrentFilter $torrentFilter)
    {
        $this->cache = $cache;
        $this->torrentFilter = $torrentFilter;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getWhenAvailable(): int
    {
        return $this->get();
    }

    /**
     * @return int
     */
    public function get(): int
    {
        // @HEYLISTEN Use the cache to retreive that value instead of doing request every time.
        return (int) floor(count($this->torrentFilter->getAllAuthenticatedUserTorrents()) / PaginationHelper::PAGINATION_MAX_RESULT);
    }
}
