<?php

declare(strict_types=1);

namespace MoustacheBundle\Helper;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use TorrentBundle\Helper\HelperInterface;

class PaginationHelper implements HelperInterface
{
    const PAGINATION_PARAMETER = 'p';
    const PAGINATION_MAX_RESULT = 24;
    const PAGINATION_DEFAULT_PAGE = 0;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @return int|null
     */
    public function getWhenAvailable()
    {
        return (int) $this->getPaginationFromRequest();
    }

    /**
     * @return int
     */
    public function get(): int
    {
        $pagination = (int) $this->getPaginationFromRequest();

        if (empty($pagination)) {
            return self::PAGINATION_DEFAULT_PAGE;
        }

        return $pagination;
    }

    /**
     * @return int
     */
    public function getCurrentItems(): int
    {
        return $this->get() * self::PAGINATION_MAX_RESULT;
    }

    /**
     * @return int|null
     */
    private function getPaginationFromRequest()
    {
        return $this->request->query->get(self::PAGINATION_PARAMETER) ?? $this->request->request->get(self::PAGINATION_PARAMETER);
    }
}
