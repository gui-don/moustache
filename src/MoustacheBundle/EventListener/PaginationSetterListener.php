<?php

declare(strict_types=1);

namespace MoustacheBundle\EventListener;

use MoustacheBundle\Helper\PaginationHelper;
use MoustacheBundle\Helper\PaginationTotalPageHelper;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use TorrentBundle\Helper\AuthenticatedUserHelper;
use Twig_Environment;

/**
 * Passes pagination variables to the view.
 */
final class PaginationSetterListener
{
    /**
     * @var PaginationHelper
     */
    private $paginationHelper;

    /**
     * @var PaginationTotalPageHelper
     */
    private $paginationTotalPageHelper;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var AuthenticatedUserHelper
     */
    private $authenticatedUserHelper;

    /**
     * @param PaginationHelper          $paginationHelper
     * @param PaginationTotalPageHelper $paginationTotalPageHelper
     * @param Twig_Environment          $twig
     * @param AuthenticatedUserHelper   $authenticatedUserHelper
     */
    public function __construct(PaginationHelper $paginationHelper, PaginationTotalPageHelper $paginationTotalPageHelper, Twig_Environment $twig, AuthenticatedUserHelper $authenticatedUserHelper)
    {
        $this->paginationHelper = $paginationHelper;
        $this->paginationTotalPageHelper = $paginationTotalPageHelper;
        $this->twig = $twig;
        $this->authenticatedUserHelper = $authenticatedUserHelper;
    }

    /**
     * @param GetResponseEvent $event
     *
     * @return GetResponseEvent|null
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->authenticatedUserHelper->getWhenAvailable()) {
            return null;
        }

        $this->twig->addGlobal('pagination', [
            'currentPage' => $this->paginationHelper->get(),
            'totalPage' => $this->paginationTotalPageHelper->get(),
        ]);

        return $event;
    }
}
