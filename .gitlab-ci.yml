stages:
  - static_analysis
  - test
  - test_edge

image: registry.gitlab.com/gui-don/moustache-ci-docker:7.4

cache:
  paths:
    - vendor/

variables:
  SYMFONY_ENV: 'test'

before_script:
  - cp app/config/test/parameters.yml app/config/parameters.yml
  - php bin/composer.phar install -o --no-interaction

# Static analysis

lint_check:
  stage: static_analysis
  script:
  - bin/composer.phar validate
  - php bin/console doctrine:database:create
  - php bin/console doctrine:schema:create
  - php bin/console doctrine:schema:validate --skip-sync
  - php bin/console doctrine:database:drop --force
  - php bin/console lint:twig src
  - php bin/console lint:twig app
  - php bin/console lint:yaml src
  - php bin/console lint:yaml app

code_style:
  stage: static_analysis
  script:
    - php vendor/bin/php-cs-fixer fix --allow-risky yes --dry-run

sast:
  stage: static_analysis
  script:
    - chmod +x ./vendor/pheromone/phpcs-security-audit/symlink.sh
    - ./vendor/pheromone/phpcs-security-audit/symlink.sh
    - ./vendor/bin/phpcs -p --error-severity=1 --warning-severity=6 --standard=./vendor/pheromone/phpcs-security-audit/example_base_ruleset.xml --extensions=php,inc,lib,module,info src
    - ./vendor/bin/phpcs -p --error-severity=1 --warning-severity=1 --standard=./vendor/pheromone/phpcs-security-audit/example_base_ruleset.xml --extensions=lib,module,inf vendor

stan:
  stage: static_analysis
  script:
    - vendor/bin/phpstan analyse src --level 5

# Unit Tests

.unit_template:
  stage: test
  script:
    - php vendor/bin/phpspec run --config=app/config/test/phpspec.yml test/Spec/Learning/
    - php vendor/bin/phpspec run --config=app/config/test/phpspec.yml test/Spec/MoustacheBundle/
    - php vendor/bin/phpspec run --config=app/config/test/phpspec.yml test/Spec/TorrentBundle/

unit:7.0:
  extends: .unit_template
  stage: test_edge
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.0
  allow_failure: true
  only:
    variables:
      - $SCHEDULE == "edge"

unit:coverage:
  stage: test
  image: registry.gitlab.com/gui-don/moustache-ci-docker:coverage
  script: vendor/bin/phpspec run --config=app/config/test/phpspec_coverage.yml
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'

unit:7.1:
  extends: .unit_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.1

unit:7.2:
  extends: .unit_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.2

unit:7.3:
  extends: .unit_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.3

unit:7.4:
  extends: .unit_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.4

unit:rc:
  extends: .unit_template
  stage: test_edge
  image: registry.gitlab.com/gui-don/moustache-ci-docker:rc
  allow_failure: true
  only:
    variables:
      - $SCHEDULE == "edge"

# Functional Tests

.func_template:
  stage: test
  before_script:
    - redis-server --daemonize yes
    - cp app/config/test/parameters.yml app/config/parameters.yml
    - php bin/composer.phar install -o --no-interaction
  script:
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Connection
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Error
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Authentication
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Content
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Message
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Add
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Remove
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Share
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Download
    - php vendor/bin/behat --config app/config/test/behat.yml --suite=moustache_suite --tags Status

func:7.1:
  extends: .func_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.1

func:7.2:
  extends: .func_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.2

func:7.3:
  extends: .func_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.3

func:7.4:
  extends: .func_template
  image: registry.gitlab.com/gui-don/moustache-ci-docker:7.4

func:rc:
  extends: .func_template
  stage: test_edge
  image: registry.gitlab.com/gui-don/moustache-ci-docker:rc
  allow_failure: true
  only:
    variables:
    - $SCHEDULE == "edge"
