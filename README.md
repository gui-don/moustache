## Moustache
##### A multi-client, multi-user torrent web application
| Pipeline | Code Quality | Coverage |
|:--------:|:------------:|:--------:|
[![pipeline status](https://gitlab.com/gui-don/moustache/badges/master/pipeline.svg)](https://gitlab.com/gui-don/moustache/commits/master) | [![Scrutinizer Code Quality](https://scrutinizer-ci.com/gl/guidon/gui-don/moustache/badges/quality-score.png?b=master&s=8885e21a5de88c36656a5054e9e84ca88c91cfd1)](https://scrutinizer-ci.com/gl/guidon/gui-don/moustache/?branch=master) | [![Code Coverage](https://scrutinizer-ci.com/gl/guidon/gui-don/moustache/badges/coverage.png?b=master&s=8a2a35ace117b33379475937176a3b3a6a80267a)](https://scrutinizer-ci.com/gl/guidon/gui-don/moustache/?branch=master)


| PHP 7.4 | PHP 7.3 | PHP 7.2 | PHP 7.1 | PHP 7.0 | Older PHP |
|:-------:|:-------:|:-------:|:-------:|:-------:|:---------:|
| ✔       | ✔       | ✔       | ✔       | ✖       | ✖         |

see: [Latest builds](https://gitlab.com/gui-don/moustache/-/jobs?scope=finished)

--------------------------------------------------------------------------------

<p align="center"><img width="120" src="moustache.png" /></p>

It’s KISS. It’s elegant. Like a Moustache.

Moustache is a web application that act as a middleware and front-end for a back-end torrent client (transmission, deluge…).

It brings:
- Multi-users on a single instance, with separation of torrents;
- limitation of a simple user’s permission, only admin can administrate their back-end torrent client;
- direct-download torrent files from Moustache to user’s browsers;
- simple UI with a single click download.

Moustache loves you (however it’s still a software).

### Installation

- Download this project wherever you want it to be installed:


        $ cd /var/www/
        $ git clone git@github.com:gui-don/Moustache.git moustache

- Run composer to install dependencies inside the application directory (depending on your permission system, you may want to run this command as www-data user).


        $ cd /var/www/moustache
        $ SYMFONY_ENV=prod composer install -o --no-dev # If composer is installed on your system
        $ SYMFONY_ENV=prod php bin/composer.phar install -o --no-dev # Alternative

- If it is the first time, composer will ask for every values in the [Configuration](#configuration) section.

- Create database the first time:

        $ bin/console doctrine:database:create --env=prod -v --if-not-exists
        $ bin/console doctrine:schema:create --env=prod -v

### Configuration

At the end of the composer process, you will be asked for some configuration.
The configuration file is stored in `app/config/parameters.yml`.
If your PHP installation enables it, a symlink has been created in `/etc/moustache/parameters.yml` on your system. We suppose you use Linux.

| name                 | type                                    | default                          | description |
|:--------------------:|:---------------------------------------:|:--------------------------------:|:------------|
| database_name        | *String*                                | moustache                        | Name of the database.
| database_path        | *Pathname*                              | /var/lib/moustache/moustache.db3 | The database absolute path.<br> You can use the `%kernel.root_dir%` variable inside this string to relate to the installation path of moustache.
| -------------------- | --------------------                    |  --------------------            |  --------------------
| cache_name           | \[*redis*, *memcached*, *filesystem*\]  | redis                            | Caching system to be used.<br> Because cache in Moustache is highly volatile, Redis is highly recommended (needs *php-redis* extension and *redis-server* packages). Memcached is discouraged for its poor write performances (needs *php-memcached* extension and *memcached* packages). Filesystem (flat files) can be used at last resort or for testing purpose.
| cache_host           | *Ip* OR *FQDN*                          | 127.0.0.1                        | Memcached or Redis server location. This value is only used if **cache_name** is set to *memcached* or *redis*.
| cache_port           | *Integer*                               | 6379                             | Memcached or Redis server port. Only used if **cache_name** is set to *memcached* or *redis*.
| -------------------- | --------------------                    |  --------------------            |  --------------------
| secret               | *String*                                | ThisIsNotSecretPleaseChangeIt    | A secret use to salt hashes passwords. Choose a random string.
| -------------------- | --------------------                    |  --------------------            |  --------------------
| torrent_rpc_client   | \[*transmission*, *fake*\]              | transmission                     | The torrent RPC client name.
| torrent_rpc_host     | *Ip* OR *FQDN*                          | 127.0.0.1                        | The torrent RPC server location.
| torrent_rpc_port     | *Integer*                               | 9091                             | The torrent RPC port.
| torrent_storage      | *Path*                                  | /var/upload/:username:           | The path where to store downloaded torrents.<br>You can use a special variable `:username:` (careful semicolons **:** not %) within the path, which will be replace dynamically by a Moustache user’s username.<br>Careful, the path(s) must be RW for the system webserver user as well as the torrent RPC client or an error will occur at runtime.
| -------------------- | --------------------                    |  --------------------            |  --------------------
| maintenance_lock_file | *Pathname*                             | %kernel.root_dir%/../web/.down   | The path and filename of a file which, if present, will redirects all users to a 503 maintenance page.
| system_conf_dir       | *Path*                                 | /etc                             | Absolute path of the configuration files of the system. No trailing slash.
| -------------------- | --------------------                    |  --------------------            |  --------------------
| allow_direct_download            | \[*0*, *1*\]                | 1                                | Determines whether or not users are allowed to direct download their torrent.
| clean_downloads_delay_in_seconds | *Integer*                   | 18000                            | Times in seconds a file is made available after a user had hit the download button (careful, this value is checked against the file latest access time).<br>Adjust the value whether your webserver modifies atime or not…
| -------------------- | --------------------                    |  --------------------            |  --------------------
| compass_path         | *Path*                                  | /usr/bin/compass                                 | Developer option: compass path.
| sass_path            | *Path*                                  | /usr/bin/sassc                                   | Developer option: SASS path.
| uglifycss_path       | *Path*                                  | %kernel.root_dir%/../node_modules/.bin/uglifycss | Developer option: Uglify CSS path.
| uglifyjs_path        | *Path*                                  | %kernel.root_dir%/../node_modules/.bin/uglifyjs  | Developer option: Uglify JS path.

### Server administration

- PHP configuration: **7.0 or more** with the following extensions: *iconv*, *mbstring*, *intl*, *dom*, *zip* and *sqlite*. Optionally, *memcached* or *redis* extension too.
- PHP configuration: *opcache* is recommended and should be enabled by default. *8M* for *memory_limit* should be sufficient, if not *16M* will do.
- PHP configuration: follow best security practices, for instance by disabling `expose_php`, `enable_dl`, `allow_url_fopen` or `allow_url_include`.
- HTTP server: nginx, apache2 or lighthttpd are great solutions for a public server use.
- HTTP server: should rewrite all URL without extensions or with `*.php` extension to `web/app.php`.
- HTTP server: should not interpret (or pass to fastCGI) file with extension other than `*.php`.
- HTTP server: should use compression to deliver CSS, JS, fonts and images.
- If you use Moustache at home, in a local NAS for example, you can use PHP internal server:

        $ cd /path/to/moustache
        $ php -S your_ip:a_random_port -t web web/app.php

- Security: **HTTPS** is mandatory to secure Moustache. If not, consider your and other users passwords public.
- Security: Moustache does not provide built-in security against brute force attacks on login form. Use fail2ban or SSHguards.
- Security: please, refer to best security practices (HTTP server, PHP, IDS…) if you decide to put Moustache online.


### User creation

Because this application was not made to be available for anyone to register, it implements a unique way to create new users.
Administrator has to run the following command:

        $ bin/console moustache:generate:signup newUser --env=prod

Where `newUser` is the username of the person you want to be able to signup.
The command generates a unique signup link you can share with your friend so they can set a password and be able to log in.

Note: for now, it is not possible for the new user to change their username, because it might be dynamically used for torrent pathes.
Ask your friend’s preference before chosing a name.

### Troubleshooting

Logs are stored in `/path/to/moustache/var/logs`. Also, when errors related to Moustache are caught, they are sent to syslog.

### KNOWN ISSUES

- TODO: Only handle transmission client for now. Ask for new client or make a PR.
- TODO: There is no UI function to remove a torrent. `/remove/{id}` is the URL. It’ll come later.
- TODO: It’s possible that disk space availability checker ignores quota.
- TODO: Failing states (tracker errors and missing files) are not properly handled.
-
- Torrent does not appear stopped or started after doing the action, the page has to be reloaded. It happens because Moustache is asynchronous and the torrent client is slower.

### Performance

#### Caching options comparison

These are the results running the functional tests, adding latency of exactly 100ms for each client RPC calls:


| No cache (not an option) | Memcached                | Filesystem               | Redis                    |
|:------------------------:|:------------------------:|:------------------------:|:------------------------:|
| ~1m34s                   | ~1m36s                   | ~35s                     | ~30s                     |

However, this does not strictly represent reality because tests does not correctly represent usage.

The surprising results about memcached comes from Moustache highly volatile cache coupled with Memcached very slow write operations.
Also, with concurrent calls, Memcached will be more efficient than no cache at all.

Although filesystem is a pretty good solution, it can leads to lots of I/O on disks when used with Moustache.

Redis is the recommended solution.

### Security

#### Download button disabled

To prevent malicious torrents to become remotely executable, some file are not downloadable from the web interface (download button is disabled).
For now, authorized download files are whitelisted within Moustache code.

#### Web server configuration

Concerning previous point, it’s essential for the web server administrator of Moustache **NOT** to let the Apache, Nginx or whatever interpret fancy file extensions.
Moustache needs web server software to handle `.php`, that is all. Ideally, it should only be able to interpret `web/app.php`.
Of course, if you run other web applications with the same server, it can also handle `.py`, `.asp` or whatever genuine script you want.

Here is an exemple of a **BAD CONFIGURATION**: making your server interpret `.ogg` files. It’s very common sens. Ignoring this warning will make Moustache (and your server) highly vulnerable as it can expose such files.
